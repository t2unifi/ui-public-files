FROM nginx:1.25.3-alpine as builder

RUN apk update && apk add git && echo "machine bitbucket.org \n login t2jenkins \n password ${bitbucketCred}" >/root/.netrc && git clone https://bitbucket.org/t2unifi/ui-public-files.git

FROM nginx:1.25.3-alpine
## Copy our default nginx config
COPY default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/* 


## From �builder� stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ui-public-files /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
